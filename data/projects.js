var projects =[
    {
        'id':1,
        'title':'A Service Oriented Framework for Smart Hospitals ',
        'project_incharge':['Dr. Vidhya Balasubramanian'],
        'co_project_incharge':['Dr. C.K.Shyamala ','Dr. G.Jeyakumar'],
        'start_date':'February 2, 2017',
        'end_date':'January 31, 2019',
        'department':'Computer Science',
        'school':'School of Engineering',
        'funding_agency':['NRDMS', 'DST'],
        'image':'/images/projects/smart-hospitals.jpg',
        'description':'Asset and people tracking solutions are crucial in providing seamless operations devoid of clashes between services and elements, especially when designing smart environments for large hospitals. Here monitoring mobile patients, locating important personnel, detecting and locating crowds, locating and tracking essential surgical assets and verification of access control restrictions with respect to people and assets, are important functionalities to be provided by the smart space. In this project, we aim to achieve the above by developing a low-cost, end-to-end system that can help configure and operate a smart hospital, which supports the above functionalities. The proposed system includes a sensor infrastructure, smart algorithms for asset and people tracking applications over this infrastructure and the underlying database system to support these applications. Research focus includes development of effective data management and representation of real-time streams of moving objects and sensor data, cost effective localization, ambient sensing and intelligence, and distributed inferencing.'
    },
    {
        'id':2,
        'title':'Indoor Information Representation and Management System (IIRMS)',
        'project_incharge':['Dr. Vidhya Balasubramanian'],
        'co_project_incharge':['Dr. Latha Parameswaran'],
        'start_date':'September 1, 2012',
        'end_date':' December 31, 2015',
        'department':'Computer Science',
        'school':'School of Engineering',
        'funding_agency':['NRDMS', 'DST'],
        'image':'/images/projects/iirms_building.png',
        'description':'An indoor information system is a system that gives information about the details of a building such as the different spaces within the building, exits, and their properties and also about other entities in the buildings like utilities, objects like tables, shelves etc. It not only allows the users to view the building layout but also allows end-users to query the system for information like exit ways, paths, location of rooms etc. Existing indoor databases are primarily developed for specific applications like navigation systems, 3D visualization of buildings etc. The goal of this project is to create a hierarchical extendible data model for indoor information which can represent buildings, entities within it, utilities, objects inside the building, and sensors inside the building for object tracking and localization, so that the represented data is easily stored and queried using a spatio-temporal database. An indoor information system is being developed as part of this project which can support indoor asset tracking and real-time navigation over this data model. In addition novel indoor localization algorithms are being developed for people tracking and asset tracking. These algorithms aim to use cost efficient technologies and are designed to improve the accuracy of indoor people and asset tracking applications. The project was funded by NRDMS, DST, India (NRDMS/11/1925/012) and was completed in December 2015.',
    },
    {
        'id':3,
        'title':'Customizable Product Recommender Systems',
        'project_incharge':['Dr. Vidhya Balasubramanian'],
        'co_project_incharge':null,
        'start_date':null,
        'end_date':null,
        'department':'Computer Science',
        'school':'School of Engineering',
        'funding_agency':null,
        'image':'/images/projects/multidimensional-data-analytics-lab-amuda.jpg',
        'description':'Current product recommender systems only allow users to select from off the shelf products. However if they wish to mix and match components to create customized products, current recommendation systems do not support that. For instance based on user requirements current systems may suggest different desktop computers, however they cannot suggest configurations that allow users to add a different memory card or additional hard disk. Our goal in this project is to expand the way recommendation systems work so that they can suggest the products along with their customizable components to customers and give them a greater choice in shopping. As a part of this project new models to represent this problem and  novel and efficient solutions for the same are being developed.'

    },
    {
        'id':4,
        'title':'Statistical Approaches to Generating Concept Graphs',
        'project_incharge':['Dr. Vidhya Balasubramanian'],
        'co_project_incharge':null,
        'start_date':null,
        'end_date':null,
        'department':'Computer Science',
        'school':'School of Engineering',
        'funding_agency':null,
        'image':'/images/projects/knowledge-representation.jpg',
        'description':'The goal of this project is to improve the ease of generating knowledge representations, so that semantic search can be better supported. The project aims to simplify the process of knowledge representation by using statistical approaches and enhancing their expressivity using graph theoretical approaches. A GUESS based extension has been designed to support the creation of concept graphs and for semantic analysis of these concept graphs.'

    },
    {
        'id':5,
        'title':'ACIST : Amrita Content based Indexing and Searching Tool',
        'project_incharge':['Dr. Vidhya Balasubramanian'],
        'co_project_incharge':null,
        'start_date':null,
        'end_date':null,
        'department':'Computer Science',
        'school':'School of Engineering',
        'funding_agency':['MHRD', 'E-Learning Research Center, Amritapuri.'],
        'image':'/images/projects/acist1.jpg',
        'description':'This project is a lecture browser system that helps users search a large corpus of lecture videos efficiently. This system helps the user to search the video both at the video level and segment level. A prototype system has been developed and has been hosted on our servers for testing. In addition better metadata extraction approaches have been developed. A tool has been developed based on TESSERACT and GOCR that extracts text from slides that are embedded in the lecture video. Using the extracted content from the audio and video modalities an effective approach for keyphrase extraction and lecture segmentation  has been developed. The keyphrases and segments generated by this approach help summarize the content of the lecture videos and aid in better content based search and retrieval.'
    }    
]