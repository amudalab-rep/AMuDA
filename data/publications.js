var publications=[
    {
        'id':1,
        'title':'A Scalable and Hybrid Location Estimation Algorithm for Long-Range RFID Systems',
        'publication_type':'Journal Article',
        'authors':['Dharani Tejaswini K', 'Dr. Vidhya Balasubramanian'],
        'source':'International Journal of Wireless Information Networks, Volume 25, Number 2, p.186–199 (2018)',
        'url':'https://doi.org/10.1007/s10776-018-0394-3',
        'abstract':'Localization of assets is a critical component of smart building applications. This problem involves a lot of challenges and gets even more challenging when a consistently high level of accuracy has to be ensured while employing long-range RFID readers. In general, these antennas are placed so that there is some level of overlap of their ranges of influence, in order to support asset tracking with high precision. However, it is desirable to reduce the level of overlap when employing multiple readers for large-scale asset tracking, due to the high cost of these RFID readers and antennas. In such scenarios, it is important to maintain accuracy while increasing the coverage area of the readers, and for this a robust localization algorithm is necessary. This paper describes a novel hybrid indoor localization algorithm that combines the transmission power level control and the signal strength information in an intelligent manner to locate assets accurately. The algorithm does not require much calibration and is easily scalable. Additionally, it allows for both coarse and fine-grained location estimation depending on application requirements. Our experiments in real indoor environments, and with different degrees of overlap between long-range RFID antennas show that our algorithm provides higher accuracy in comparison to other algorithms, and is also consistent.',
        'cite_this':'D. K. Tejaswini and Dr. Vidhya Balasubramanian, “A Scalable and Hybrid Location Estimation Algorithm for Long-Range RFID Systems”, International Journal of Wireless Information Networks, vol. 25, pp. 186–199, 2018.'
    },
    {
        'id':2,
        'title':'Localization Algorithm for Large-Scale RFID based Asset Tracking',
        'publication_type':'Thesis',
        'authors':['Dharani Tejaswini K'],
        'source':'Amrita Vishwa Vidyapeetham (2017)',
        'url':null,
        'abstract':null,
        'cite_this':'D. Tejaswini K, “Localization Algorithm for Large-Scale RFID based Asset Tracking”, Amrita Vishwa Vidyapeetham, 2017.'
    },
    {
        'id':3,
        'title':'A Robust Approach for Improving the Accuracy of IMU based Indoor Mobile Robot Localization',
        'publication_type':'Conference Paper',
        'authors':['Murthy , Surya D', 'S, Srivenkata Krishnan',' G, Sundarrajan','S, Kiran Kassyap','Dr. Vidhya Balasubramanian'],
        'source':'13th International Conference on Informatics in Control, Automation and Robotics, Scitepress (2016)',
        'url':null,
        'abstract':null,
        'cite_this':'S. D. Murthy, S, S. Krishnan, G, S., S, K. Kassyap, and Dr. Vidhya Balasubramanian, “A Robust Approach for Improving the Accuracy of IMU based Indoor Mobile Robot Localization”, in 13th International Conference on Informatics in Control, Automation and Robotics, 2016.'
    },
    {
        'id':4,
        'title':'An Optimization framework for Solving RFID Reader Placement Problem Using Differential Evolution Algorithm',
        'publication_type':'Conference Proceedings',
        'authors':['N.Rubini', 'C.V.Prasanthi', 'S.Subanidha', 'G.Jeyakumar'],
        'source':'Communication and Signal Processing (ICCSP), 2017 International Conference on (2017)',
        'url':null,
        'abstract':'A RFID (Radio-Frequency Identification) system comprises of an arrangement of RFID readers and RFID tags. A RFID system is known for its quick identification of objects to which the RFID tags are attached. RFID system has been used mainly in industries for identifying and tracking important assets. The most essential property of a RFID system is deploying RFID readers to ensure total coverage of all the RFID tags in an area. The total cost of an RFID system relies mostly on the number of readers. Thus, finding an optimal number of readers and their positions to cover all tags is a standout amongst the most vital issues in a RFID system. This paper proposes a simple simulation strategy for optimal RFID reader placement for a system with RFID readers of circular coverage. This simulation is carried with Differential Evolution (DE) algorithm, which has achieved 100% coverage with optimal number of RFID readers. The experimental set up and the results are explained in this paper.',
        'cite_this':'N. Rubini, Prasanthi, C. V., Subanidha, S., Dr. Vidhya Balasubramanian, and Jeyakumar, G., “An Optimization Framework for Solving RFID Reader Placement Problem Using Differential Evolution Algorithm”, Communication and Signal Processing (ICCSP), 2017 International Conference on. 2017.'
    },
    // {
    //     'id':5,
    //     'title':
    //     'publication_type':
    //     'authors':
    //     'source':
    //     'url':
    //     'abstract':
    //     'cite_this':
    // },
]