var url = $(document)[0].URL.toString();
if(url.includes('file:///')){
url = url.split('.')[0]
url =url.replace('/index','');
}
console.log($(document));

$(document).ready(function(){
// window.location = url+'/sample.html';
// $('body').append('<p>Sample</p>')
navBarPeopleClick('student'); 
    $(".nav-link-people").click(function(){
       navBarPeopleClick($(this)[0].id)
       return false;
    })
navBarProjectsClick();
$('#heading0').removeClass('collapsed')
for (var i=1; i<projects.length; i++){
    // $('#collapse'+i).addClass('collapsed')
    $('#collapse'+i).collapse("hide");
}
navBarPublicationsClick();
})

var navBarPeopleClick = function(people_type){
    $(".people").empty();
    $(".nav-link-people").removeClass('active');
    $("#"+people_type).addClass('active');
    $(this).addClass('active');
    var peopleElement = $('<div class="row">')
    for(var i=0;i<people.length;i++){
        if(people[i].type === people_type){
            var image = url;
            var peopleElementColumn = $('<div class="card col-sm">')
            if(people[i].image){
                image+=people[i].image;
            }else{
                image+='/images/people/default-people.png'
            }
            var peopleElementCardImage = $('<img class="card-img-top" src="'+image+'" alt="Card image cap">')
            var peopleElementCardBody = $('<div class="card-body">');
            var peopleElementCardTitle = $('<h5 class="card-title">');
            peopleElementCardBody.append(peopleElementCardTitle);
            if(people[i].profile !=null){
                peopleElementCardTitle.append('<a class = "card-link person-name" href="'+people[i].profile+'">'+people[i].name+'</a>');
            }
            else{
                peopleElementCardTitle.append(people[i].name);
            }
            if(people[i].member != null){
                peopleElementCardBody.append('<h6 class="card-subtitle mb-2 text-muted">'+people[i].member+'</h6>')

            }
            peopleElementCardBody.append('<p class="card-text">'+people[i].designation+' at department of '+people[i].department+',<br/>'+people[i].institution+', '+people[i].location+'</p>')
            if(people[i].google_scholar != null){
                peopleElementCardBody.append('<a href="'+people[i].google_scholar+'" class="card-link">Google Scholar</a>')
            }
            peopleElementColumn.append(peopleElementCardImage)
            peopleElementColumn.append(peopleElementCardBody)
            peopleElement.append(peopleElementColumn)}
    }
    peopleElement.appendTo(".people");
    return false;
}

var navBarProjectsClick = function(){
    var projectSection =$('#projects_section')
    for (var i=0;i<projects.length;i++){
        var image = url+projects[i].image
        var projectTitle  = projects[i].title;
        var projectPeople = projects[i].project_incharge;
        var projectFunding='';
        if(projects[i].co_project_incharge != null){
            projectPeople = projects[i].project_incharge+"\n"+projects[i].co_project_incharge;
        }        
        if(projects[i].start_date != null && projects[i].end_date != null){
            projectTitle = projects[i].title+'  ('+projects[i].start_date+'-'+projects[i].end_date+')';
        }
        if (projects[i].funding_agency != null){
            projectFunding = projects[i].funding_agency;
        }
        projectSection.append('<div class="accordion" >'+
        '<div class="card col-sm">'+
          '<div class="card-header collapsed" id="heading'+i+'" data-toggle="collapse" data-target="#collapse'+i+'" aria-expanded="false" aria-controls="collapse'+i+'">'+
            '<h6 class="mb-0">'+
                    projectTitle+
            '</h6>'+
          '</div>'+
      
          '<div id="collapse'+i+'" class="collapse show" aria-labelledby="heading'+i+'" data-parent="#accordionExample">'+
          '<img class="card-img-top" src="'+image+'" alt="Card image cap">'+
            '<div class="card text-center">'+
            '<div class="card-body">'+
              projects[i].description+
              '</div>'+
              '<div class="card-footer text-muted">'+
              '<p class= "text-right" style="float:right">'+
              projectFunding+
                '</p>'+
                '<p class= "text-left" style="float:left">'+
                projectPeople+
                '</p>'+
                '</div>'+
            '</div>'+
          '</div>'+
        '</div>' )
    }
}


var navBarPublicationsClick =  function(){
    console.log('called')
    var publicationBody = $('.publicationsTable');
    for(var i=0;i<publications.length;i++){
        publicationBody.append( '<tr>'+
            '<th scope="row">'+publications[i].id+'</th>'+
            '<td>'+publications[i].title+'</td>'+
            '<td>'+publications[i].publication_type+'</td>'+
            '<td>'+publications[i].authors+'</td>'+
            '<td>'+publications[i].source+'</td>'+
          '</tr>')
    }
}