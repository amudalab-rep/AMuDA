var people = [
    {
      "id": 1,
      "name": "Dr. Vidhya Balasubramanian",
      "designation": "Professor",
      "department":"Computer Science",
      "institution":"School of Engineering",
      "location":"Coimbatore",
      "university":"Amrita Vishwa Vidhyapeetham",
      "profile": "https://www.amrita.edu/faculty/b-vidhya",
      "google_scholar":"https://scholar.google.co.in/citations?user=0Kkr6WQAAAAJ&hl=en",
      "image": "/images/people/VIDHYA-BALASUBRAMANIAN.jpg",
      "member":"Faculty Member",
      "type":"faculty"
    },
    {
        "id": 2,
        "name": "Staff 1",
        "designation": "JRF",
        "department":"Computer Science",
        "institution":"School of Engineering",
        "location":"Coimbatore",
        "university":"Amrita Vishwa Vidhyapeetham",
        "member":"Staff Member",
        "type":"staff"
      },

      {
        "id": 3,
        "name": "Student 1",
        "designation": "II B.Tech",
        "department":"Computer Science",
        "institution":"School of Engineering",
        "location":"Coimbatore",
        "university":"Amrita Vishwa Vidhyapeetham",
        "member":"Student Member",
        "type":"student"
      }
  ]